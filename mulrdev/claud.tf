terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}



variable "subdomein" {
  default = "domein.com"
}

resource "cloudflare_record" "subdomein" {
  zone_id = var.zone_id
  name    = "subdomein"
  value   = "127.0.0.1"
  type    = "A"
  proxied = false
}

